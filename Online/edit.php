<?php
	require_once('config.php');
	require_once('functions/function.php');
	get_header();
	$id=$_GET['e'];
	$sele="SELECT * FROM cit_student WHERE stu_id='$id'";
	$Q=mysqli_query($con,$sele);
	$info=mysqli_fetch_array($Q);
?>
            <div class="body_part">
                <h2>Student Registration</h2>
                <form action="" method="post">
                    <table class="reg_table" border="0" cellpadding="0" cellspacing="10">
                        <tr>
                            <td>Student Name</td>
                            <td>:</td>
                            <td>
                                <input type="text" name="name" value="<?= $info['stu_name'];?>" id="" class="input_field"/>
                            </td>
                        </tr>
                         <tr>
                            <td>Student Email</td>
                            <td>:</td>
                            <td>
                                <input type="email" name="email" id="" class="input_field"/>
                            </td>
                        </tr>                   
                        <tr>
                            <td>Student Phone</td>
                            <td>:</td>
                            <td>
                                <input type="text" name="phone" id="" class="input_field"/>
                            </td>
                        </tr>
                        <tr>
                            <td>Student Roll</td>
                            <td>:</td>
                            <td>
                                <input type="text" name="roll" id="" class="input_field"/>
                            </td>
                        </tr>
                        <tr>
                            <td>Student Department</td>
                            <td>:</td>
                            <td>
                                <input type="text" name="department" id="" class="input_field"/>
                            </td>
                        </tr>
                        <tr>
                            <td>Student Session</td>
                            <td>:</td>
                            <td>
                                <input type="text" name="session" id="" class="input_field"/>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>
                                <input type="submit" value="UPDATE" id="" class="submit"/>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
<?php
	require_once('includes/footer.php');
?>